import numpy as np
import tensorflow as ft
import keras.backend as K
import matplotlib.pyplot as plt
from keras.layers import Input, Dense, Flatten
from keras.models import Model
from keras.losses import mse
from keras.callbacks import TensorBoard

if __name__ == '__main__':
    # Accés aux données
    def gen(batch_size):
        while True:
            X = np.random.normal(size=(batch_size, 8, 8))
            Y = np.random.normal(size=(batch_size, 1))
            yield X, Y
    X, Y = gen(32).__next__()

    # Construction d'un modèle simple
    input = Input(shape=(8, 8), name='in')
    input_target = Input(shape=(10, 1), name='in_target')
    input_flatten = Flatten()(input)
    hidden = Dense(32)(input_flatten)
    output = Dense(1, name='output')(hidden)

    # Pertes
    def custom_loss(perte):
        def loss(y_pred, y_true):
            tutu = K.variable(0)
            for i in range(32):
                if np.max(perte[i, :, 0]) == 1:
                    tutu = tutu + (y_pred[i] + y_true[i])*0.00001
                else:
                    tutu = y_pred[i] + y_true[i]
            return tutu
        return loss

    model = Model(inputs=[input, input_target], outputs=output)
    model.summary()
    model.compile(optimizer='adam', loss=custom_loss(input_target))
    perte = np.ones((32, 10, 1))
    print(perte.shape)
    feed_dict_in = {'in': X+1, 'in_target': perte}
    loss = model.train_on_batch(feed_dict_in, Y)
    print(loss)
    tensorBoard = TensorBoard('tensorboard.logs')
    loss = model.fit(feed_dict_in, Y, epochs=1, steps_per_epoch=1, callbacks=[tensorBoard])
