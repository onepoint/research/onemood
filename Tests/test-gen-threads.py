from keras.models import Sequential
from keras.layers import Dense
import numpy as np
import time
import threading


class ThreadSafeIter:
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """
    def __init__(self, it):
        self.it = it
        self.lock = threading.Lock()

    def __iter__(self):
        return self

    def __next__(self):
        with self.lock:
            return next(self.it)


# On définit ici le décorateur, celui-ci initialise et retourne un nouvel objet générateur
# Il retourne comme prévu la définition d'une méthode (le nouveau comportement de notre méthode à modifier)
def threadsafe_generator(f):
    """A decorator that takes a generator function and makes it thread-safe.
    """
    def g(*a, **kw):
        return ThreadSafeIter(f(*a, **kw))
    return g

# On utilise un décorateur pour modifier le comportement par défaut de la fonction génératrice
# Le comportement supplémentaire apporté est la gestion du thread pour l'execution d'une génération d'un batch
@threadsafe_generator
def gen():
    """Si l'on souhaite utiliser le generateur naif on enleve le décorateur"""
    print("Nouveau generateur initié")
    i = 0
    while True:
        x = np.ones((10, 2))
        y = np.ones(10)
        print('generator yield batch:' + str(i))
        i += 1
        yield x, y


if __name__ == '__main__':

    model = Sequential()
    model.add(Dense(10, input_shape=(2,)))
    model.add(Dense(10000000))
    model.add(Dense(1, input_shape=(2,)))
    model.compile(optimizer='RMSprop', loss='mean_squared_error', metrics=['acc'], )
    # model.summary()
    # gen = gen1()
    # Dans cette congig le générateur génère un batch dés qu'il le peut
    # En ralentissant on remarque que le modele peut être en attente de la génération d'un batch
    # model.fit_generator(generator=gen, steps_per_epoch=10, epochs=5, use_multiprocessing=False, max_queue_size=3)

    # Cant pickle generator objects.... problème d'encapsulation : utilisation d'une méthode Keras pas au bon endroit
    # model.fit_generator(generator=gen, steps_per_epoch=10, epochs=5, use_multiprocessing=True, max_queue_size=3)

    # Generator already executing + Your generator is NOT thread-safe issue #1638
    # model.fit_generator(generator=gen, steps_per_epoch=10, epochs=5, use_multiprocessing=False,
    # max_queue_size=3, workers=3)

    # On utilise un generator bloquant l'execution sur plusieurs threads
    # Dans ce fichier test, no probleme ça maaaarche
    gen = gen()
    print(type(gen))
    model.fit_generator(generator=gen, steps_per_epoch=10, epochs=5, use_multiprocessing=False, workers=10, max_queue_size=10)