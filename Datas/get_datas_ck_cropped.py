import os
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt
from glob import glob
import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Conv2D, Flatten, Dense, BatchNormalization, ReLU, MaxPool2D
from Datas.thread_safe import threadsafe_generator
from math import *


def init_disc_generator(params, auto=False):
    """
    Retourne un générateur et le nombre de batch par epoch et deux générateurs (un test et un pour la validation)
    :param params : dictionnaire possedant les id dir_path, batch_size, img_height, img_width, shuffle
    :param auto:
    :return:
    """
    if auto:
        file_count = len(glob(os.path.join(params['dir_path'], '*', '*')))
    else:
        file_count = len(glob(os.path.join(params['dir_path'], '*', '*')))
    print(file_count)

    steps_per_epoch = (file_count*(1-params['validation_split'])) // params['batch_size']
    validation_steps = 1
    # Création d'un objet Keras.ImageDataGenerator
    # Format des images de sortie:
    # Niveau de gris entre 0 et 1, avec zoom, variation d'intensité et rotation
    # 10% des images pour la validation
    datagen = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0,
        zoom_range=0,
        rotation_range=0,
        validation_split=params['validation_split'])
    # Création du générateur avec les paramètres en entrées
    train_generator = datagen.flow_from_directory(
        directory=params['dir_path'],
        target_size=(params['img_height'], params['img_width']),
        batch_size=params['batch_size'],
        shuffle=params['shuffle'],
        color_mode='grayscale',
        class_mode=params['class_mode'],
        subset="training")

    test_generator = datagen.flow_from_directory(
        directory=params['dir_path'],
        target_size=(params['img_height'], params['img_width']),
        batch_size=round(file_count*params['validation_split']),
        shuffle=False,
        color_mode='grayscale',
        class_mode=params['class_mode'],
        subset="validation")
    return train_generator, test_generator, steps_per_epoch, validation_steps


def init_auto_generator(params_in, params_out):
    """
    Retourne un générateur et le nombre de batch par epoch et deux générateurs (un test et un pour la validation)
    :param params : dictionnaire possedant les id dir_path, batch_size, img_height, img_width, shuffle
    :return:
    """
    file_count = len(glob(os.path.join(params_in['dir_path'], '*', '*')))
    steps_per_epoch = file_count // params_in['batch_size']
    # Création d'un objet Keras.ImageDataGenerator
    # Format des images de sortie:
    # Niveau de gris entre 0 et 1, avec zoom, variation d'intensité et rotation
    # 10% des images pour la validation
    datagen = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0,
        zoom_range=0,
        rotation_range=0,
        validation_split=0)
    # Création du générateur avec les paramètres en entrées
    train_generator = datagen.flow_from_directory(
        directory=params_in['dir_path'],
        target_size=(params_in['img_height'], params_in['img_width']),
        batch_size=params_in['batch_size'],
        shuffle=params_in['shuffle'],
        color_mode='grayscale',
        class_mode=params_in['mode'],
    )
    target_generator = datagen.flow_from_directory(
        directory=params_out['dir_path'],
        target_size=(params_out['img_height'], params_out['img_width']),
        batch_size=params_out['batch_size'],
        shuffle=params_out['shuffle'],
        color_mode='grayscale',
        class_mode=params_out['mode'],
    )
    @threadsafe_generator
    def combine_generator(gen1, gen2):
        while True:
            yield (gen1.next(), gen2.next())

    return combine_generator(train_generator, target_generator), steps_per_epoch

# @TODO automatiser le batch size de test
def init_auto_darkmood(params_in, params_out):
    """
    :param params : dictionnaire possedant les id dir_path, batch_size, img_height, img_width, shuffle
    :return:
    """
    file_count = len(glob(os.path.join(params_in['dir_path'], '*', '*')))
    steps_per_epoch = floor(file_count*0.8) // params_in['batch_size']
    # Création d'un objet Keras.ImageDataGenerator
    # Format des images de sortie:
    # Niveau de gris entre 0 et 1, avec zoom, variation d'intensité et rotation
    # 10% des images pour la validation
    datagen_train = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0,
        zoom_range=0,
        rotation_range=0,
        validation_split=0.2)
    # Création du générateur avec les paramètres en entrées
    train_generator = datagen_train.flow_from_directory(
        directory=params_in['dir_path'],
        target_size=(params_in['img_height'], params_in['img_width']),
        batch_size=params_in['batch_size'],
        shuffle=params_in['shuffle'],
        color_mode='grayscale',
        class_mode=params_in['mode'],
        subset='training'
    )
    test_generator = datagen_train.flow_from_directory(
        directory=params_in['dir_path'],
        target_size=(params_in['img_height'], params_in['img_width']),
        batch_size=62,
        shuffle=params_in['shuffle'],
        color_mode='grayscale',
        class_mode=params_in['mode'],
        subset='validation'
    )

    datagen_ref = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0,
        zoom_range=0,
        rotation_range=0,
        validation_split=0.2)
    train_ref_generator = datagen_ref.flow_from_directory(
        directory=params_out['dir_path'],
        target_size=(params_out['img_height'], params_out['img_width']),
        batch_size=params_out['batch_size'],
        shuffle=params_out['shuffle'],
        color_mode='grayscale',
        class_mode=params_out['mode'],
        subset='training'
    )
    test_ref_generator = datagen_ref.flow_from_directory(
        directory=params_out['dir_path'],
        target_size=(params_out['img_height'], params_out['img_width']),
        batch_size=62,
        shuffle=params_out['shuffle'],
        color_mode='grayscale',
        class_mode=params_out['mode'],
        subset='validation'
    )

    @threadsafe_generator
    def combine_generator(train_gen1, test_gen1, train_gen2, test_gen2):
        """
        train_gen1 = train_generator --> image en input, vecteur target pour l'apprentissage
        test_gen1 = train_generator --> image en input, vecteur target pour le jeu de test
        train_gen2 = target_generator --> image de référence pour l'apprentissage
        test_gen2 = target_generator --> image de référence pour le jeu de test
        :param gen1:
        :param gen2:
        :return:
        """
        while True:
            (Xtrain, Ytrain) = train_gen1.next()
            (Xtest, Ytest) = test_gen1.next()
            Xref_train = train_gen2.next()
            Xref_test = test_gen2.next()
            yield ((Xtrain, Ytrain, Xref_train), (Xtest, Ytest, Xref_test))
    return combine_generator(train_generator, test_generator, train_ref_generator, test_ref_generator), steps_per_epoch


def get_imgs_test(params):
    gen_in, _, _, _ = init_disc_generator(params)
    for batch in gen_in:
        imgs = np.array([img for img in batch]).reshape(params['batch_size'], params['img_height'],
                                                        params['img_width'], 1)
        print(imgs.shape)
        return imgs


if __name__ == '__main__':

    def tqest_gen1():
        ### Test du générateur pour classification
        dir_path = os.path.join('DB', 'CK-cropped-images', 'label_files')
        batch_size = 32
        params = {
            'dir_path': dir_path,
            'batch_size': batch_size,
            'img_height': 66,
            'img_width': 66,
            'shuffle': True,
            'mode': 'categorical',
            'validation_split': 0.2
        }
        gen, steps = init_disc_generator(params)
        model = Sequential()
        model.add(Conv2D(32, 5, padding='same', input_shape=(66,66,1)))
        model.add(BatchNormalization(momentum=0.8))
        model.add(ReLU())
        model.add(MaxPool2D(2))
        model.add(Conv2D(64, 3, padding='same'))
        model.add(BatchNormalization(momentum=0.8))
        model.add(ReLU())
        model.add(MaxPool2D(2))
        model.add(Conv2D(96, 3, padding='same'))
        model.add(BatchNormalization(momentum=0.8))
        model.add(ReLU())
        model.add(MaxPool2D(2))
        model2 = Sequential()
        model2.add(Flatten())
        model2.add(Dense(374))
        model2.add(Dense(7, activation="softmax"))
        super = Sequential()
        super.add(model)
        super.add(model2)
        super.compile(optimizer="Adam", loss=keras.losses.categorical_crossentropy, metrics=['acc'])
        super.fit_generator(gen, steps_per_epoch=10, epochs=1, verbose=1)

    def darkm_gen_test():
        dir_path_neutral = os.path.join('DB', 'CK-cropped-images', 'label_neutral_ref_files')
        dir_path_all = os.path.join('DB', 'CK-cropped-images', 'label_files')
        batch_size = 32
        params_in = {
            'dir_path': dir_path_all,
            'batch_size': batch_size,
            'img_height': 64,
            'img_width': 64,
            'shuffle': False,
            'mode': 'categorical',
            'validation_split': 0
        }
        params_out = {
            'dir_path': dir_path_neutral,
            'batch_size': batch_size,
            'img_height': 64,
            'img_width': 64,
            'shuffle': False,
            'mode': None,
            'validation_split': 0
        }
        emotions = {
            '1': "Enervement",
            '2': "Neutral",
            '3': "Dégout",
            '4': "Peur",
            '5': "Joie",
            '6': "Tristesse",
            '7': "Surprise"
        }
        gen, steps = init_auto_generator(params_in, params_out)
        truc = gen.__next__()
        (gen1, gen2) = truc
        (X, Y) = gen1
        i = 0
        while True:
            i += 1
            print(i)
            ((X, Y), Xr) = gen.__next__()
            plt.subplot(121)
            plt.imshow(X[0, :, :, 0])
            plt.title("Target: {}".format(str(emotions[str(np.argmax(Y[0])+1)])))
            plt.subplot(122)
            plt.imshow(Xr[0, :, :, 0])
            plt.title('Neutral ref')
            plt.show()
    darkm_gen_test()


    ### Test du générateur pour autoencoder
    def gen_test():
        # dir_path = os.path.join('Datas', 'DB', 'CK-cropped-images', 'all_files', 'train')
        dir_path = os.path.join('DB', 'CK-cropped-images', 'all_files', 'train')
        dir_path_neutral = os.path.join('DB', 'CK-cropped-images', 'neutral_files')
        dir_path_peak = os.path.join('DB', 'CK-cropped-images', 'peak_files')
        batch_size = 32
        params_in = {
            'dir_path': dir_path_peak,
            'batch_size': batch_size,
            'img_height': 66,
            'img_width': 66,
            'shuffle': False,
            'class_mode': 'categorical',
            'validation_split': 0
        }
        params_out = {
            'dir_path': dir_path_neutral,
            'batch_size': batch_size,
            'img_height': 64,
            'img_width': 64,
            'shuffle': False,
            'class_mode': None,
            'validation_split': 0
        }
        gen_in, steps = init_auto_generator(params_in)
        gen_out, _ = init_auto_generator(params_out)
        generator = zip(gen_in, gen_out)
        print(steps)
        i = 0
        for batch in generator:
            i += 1
            print(i)
            (set1, set2) = batch
            #if set1[:, 65, 65, 0].shape != set2[:,63,63,0].shape:
            #   print(set1[:,66,66,1].shape)
            #   print(set2[:,64,64,1].shape)
            #   print("error")
            #   break
            for imgs in batch:
                print(type(imgs))
                print(imgs.shape)
                plt.imshow(imgs[0, :, :, 0], cmap='gray')
                plt.show(1)

    #gen_test()

    #### Test de la génération des images pour suivi de l'apprentissage ####

    dir_path = os.path.join('DB', 'CK-cropped-images', 'label_files')
    batch_size = 8
    params_in = {
        'dir_path': dir_path,
        'batch_size': batch_size,
        'img_height': 66,
        'img_width': 66,
        'shuffle': False,
        'class_mode': 'categorical',
        'validation_split': 0.1
    }
    # train, test, step, val = init_generator(params_in)
    for X, Y in test:
        print(X.shape)
        for i in range(len(X[:, 65, 65, 0])):
            plt.imshow(X[i, :, :, 0])
            plt.title(str(i)+':'+str(Y[i]))
            plt.show()

    def imgs_test(params):
        gen_in, _ = init_generator(params_in)
        for batch in gen_in:
            imgs = np.array([img for img in batch]).reshape(params['batch_size'], params['img_height'],
                                                            params_in['img_width'], 1)
            print(imgs.shape)
            return imgs
    # imgs_test(params_in)


