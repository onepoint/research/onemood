import numpy as np
import matplotlib.pyplot as plt


if __name__ == '__main__':
    a_lr = np.load('lr-1030-34.npy')
    lr = [a_lr[i] for i in range(0, len(a_lr), 34)]
    print(min(a_lr))
    plt.plot(lr)
    plt.show()
