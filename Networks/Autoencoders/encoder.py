from keras.models import Sequential
from keras.layers import Conv2D, BatchNormalization, MaxPool2D, ReLU
from keras import optimizers


class Encoder:

    def __init__(self, input_shape, verbose=False):
        model = Sequential()
        model.add(Conv2D(32, 5, padding='same', input_shape=input_shape))
        model.add(BatchNormalization(momentum=0.8))
        model.add(ReLU())
        model.add(MaxPool2D(2))
        model.add(Conv2D(64, 3, padding='same'))
        model.add(BatchNormalization(momentum=0.8))
        model.add(ReLU())
        model.add(MaxPool2D(2))
        model.add(Conv2D(96, 3, padding='same'))
        model.add(BatchNormalization(momentum=0.8))
        model.add(ReLU())
        model.add(MaxPool2D(2))
        self.model = model
        if verbose:
            self.sum = self.model.summary()
