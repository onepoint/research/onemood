# coding: utf-8
import tensorflow as tf
from Networks.premodel import PreModel
from Save.logs.purge_log import *
import logging.config
from keras.backend.tensorflow_backend import set_session
from Datas.get_datas_ck_cropped import init_disc_generator, init_auto_generator, get_imgs_test
import matplotlib.pyplot as plt
import numpy as np
from Datas.get_dict_params import *  # Import des paramètre pour tous les générateurs utilisables : params_disc,
# params_test, params_autoencooder1_in, params_autoencoder1_out, params_autoencoder2

if __name__ == '__main__':
    # Configuration de la session Keras globale:
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
    session = tf.Session(config=config)
    set_session(session)

    # Initialisation des logs
    raz_logs('monitoring.log')
    logging.config.fileConfig('logging_config.ini')
    monitoring_logger = logging.getLogger('monitoring')
    console_logger = logging.getLogger('root')
    monitoring_logger.info('Loggers initialises')
    console_logger.info('loggers initialises')


    def train_disc(params_test, params_in, nbre, epochs):
        """
        Cette méthode permet de lancer plusieurs fois l'apprentissage d'un classifieur et de sauver dans des fichiers
        numpy les métrics de tous ces apprentissages ie perte/précision sur jeu d'apprentisage et jeu de test.
        :param params_test: En l'état paramètre obligatoire mais sans effet sur l'apprentissage du classifieur
        :param params_in: Paramètre pour le générateur des batchs
        :param nbre: Nombre d'apprentissage à lancer (utile pour faire des moyennes)
        :param epochs: Nombre d'epoch par apprentissage
        :return:
        """
        training_acc = []
        training_val_acc = []
        training_loss = []
        training_val_loss = []
        train_generator, test_generator, steps_per_epoch_disc, validation_steps = init_disc_generator(params_in)
        (X, Y) = test_generator.__next__()
        plt.imshow(X[0, :, :, 0])
        plt.show()

        history = []
        for i in range(nbre):
            model = PreModel(imgs_test_auto=get_imgs_test(params_test), logger=monitoring_logger, verbose=True,
                             model="init")
            # Entraînement
            history = model.discriminator_train(train_generator, test_generator, steps_per_epoch_disc, validation_steps,
                                                epochs=epochs, verbose=1)
            training_acc.append(history.history['acc'])
            training_val_acc.append(history.history['val_acc'])
            training_loss.append(history.history['loss'])
            training_val_loss.append(history.history['val_loss'])
        np.save("acc", np.array(training_acc))
        np.save("val_acc", np.array(training_val_acc))
        np.save("loss", np.array(training_loss))
        np.save("val_loss", np.array(training_val_loss))
        return history


    # Appel à la méthode d'entrainement du classifieur
    history = train_disc(params_test, params_disc, nbre=100, epochs=3000)


    def train_autoencoder1(model, params_in, params_out, epochs):
        """
        Méthode pour apprentissage du débruiteur d'émotion
        Nécessite d'initialiser 2 générateurs qui sont ensuite combinés (via une méthode thread safe de
        getdatas_ck_cropped)
        :param model:
        :param params_in: Paramètre du générateur pour les photos en entrée (avec émotion)
        :param params_out: Paramètre du générateur pour les photos en sortie  (sans émotion)
        :param epochs: Nombre d"epochs pour l'apprentissage
        :return:
        """
        # la méthode init_auto_generator retourne un générateur et le nombre de batchs par epoch
        generator, steps_per_epoch = init_auto_generator(params_in, params_out)

        # entrainement
        model.autoencoder_train(generator, steps_per_epoch=steps_per_epoch, epochs=epochs, verbose=1,
                                recons=False)
        return

    # On instancie un premodel pour mettre en place l'entrainement de l'autoencoder débruiteur
    # model = PreModel(imgs_test_auto=get_imgs_test(params_test), logger=monitoring_logger, verbose=True,
    #                 model="train-144")
    # train_autoencoder1(model, params_autoencoder1_in, params_autoencoder1_out, epochs=1000)


    def train_autoencoder2(model, params_in, epochs):
        """
        Cette méthode est capable d'entrainer un autoencoder pour une tâche de reconstruction (classique)
        :param model:
        :param params_in: Paramètre du générateur pour l'autoencodeur sur une tâche de reconstruction
        :param epochs:
        :return:
        """
        # On utilise init_disc car pas besoin d'utiliser 2 générateurs, Keras propose déjà un générateur prévu pour les
        # autoencoders classiques (reconstruction de la même image)
        train_generator, _, steps_per_epoch, _ = init_disc_generator(params_in, auto=True)
        model.autoencoder_train(train_generator, steps_per_epoch=steps_per_epoch, epochs=epochs, verbose=1,
                                recons=True)
        return

    # On instancie un premodel pour mettre en place l'entrainement de l'autoencoder reconstructeur
    model = PreModel(imgs_test_auto=get_imgs_test(params_test), logger=monitoring_logger, verbose=True,
                     model="train-1000")
    train_autoencoder2(model, params_autoencoder2, epochs=3000)

    def save_history(history):
        print(history.history.keys())
        # summarize history for loss
        plt.figure(1)
        plt.plot(history.history['loss'])
        # plt.plot(history.history['val_loss'])
        plt.title('model loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['Train', 'Test'], loc='upper left')
        plt.savefig('Loss.png')
        # summarize history for categorical accuracy
        plt.figure(2)
        plt.plot(history.history['acc'])
        plt.plot(history.history['val_acc'])
        plt.title('model accuracy')
        plt.ylabel('acc')
        plt.xlabel('epoch')
        plt.legend(['Train', 'Test'], loc='upper left')
        plt.savefig('Acc.png')
    # save_history(history)
    # model.test_video()
