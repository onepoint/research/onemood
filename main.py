# coding: utf-8
import tensorflow as tf
from Save.logs.purge_log import *
import logging.config
from keras.backend.tensorflow_backend import set_session
from Datas.get_datas_ck_cropped import init_auto_darkmood
from Datas.get_dict_params import *  # Import des paramètre pour tous les générateurs utilisables : params_disc,
# params_test, params_autoencooder1_in, params_autoencoder1_out, params_autoencoder2
from Networks.darkmood import DarkMood
from keras.callbacks import TensorBoard


if __name__ == '__main__':

    # Configuration de la session Keras globale:
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True  # gère l'accès à la mémoire du GPU de manière dynamique
    session = tf.Session(config=config)
    set_session(session)

    # Initialisation des logs
    raz_logs('monitoring.log')
    logging.config.fileConfig('logging_config.ini')
    monitoring_logger = logging.getLogger('monitoring')
    monitoring_logger.info('Loggers initialises')
    tensorBoard = TensorBoard('tensorboard.logs')

    # Initialisation des générateurs pour l'accès aux images ainsi que le nombre de batch par epoch
    # L'objet générateurs contient un générateur de tuple (Input, Target) ET un générateur d'images de référéence
    generateurs, steps = init_auto_darkmood(params_in_darkmood, params_out_darkmood)

    # Création de l'objet Darkmood pour l'apprentissage complet
    darkmood = DarkMood(tensorBoard, dict_emotion=params_emotion, steps_per_epoch=steps,
                        batch_size=32, logger=monitoring_logger, learning_rate=0.0001)
    # Entraînement de darkmood:
    darkmood.train(generateurs, 1500)
